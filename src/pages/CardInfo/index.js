import React, {PureComponent} from 'react'
import { withFirebase } from '../../context/fireBaseContext'
import { Typography, Spin, Space } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { Redirect }  from 'react-router-dom'


import s from '../Login/Login.module.scss'

const { Title, Text } = Typography;
const antIcon = <LoadingOutlined style={{fontSize: 100}}/>;

class CardInfo extends PureComponent {

  state = {
    word: {
      _id: 0,
      rus: '',
      eng: ''
    }
  }

  componentDidMount() {
    const {firebase, match: {params}} = this.props
    firebase.getUserCurrentCardRef(params.id).once('value').then(res => {
      const result = res.val()
      console.log('result =', result)
      this.setState({word: !result ? {_id: '###'} : result})
    })
  }

  render() {
    const {word: {_id, rus, eng}} = this.state
    console.log('_id =', _id)
    if (_id==='###') return (
      <Redirect to="/" />
    )


    if (rus === '' && eng === '') {
      return <div className={s.root}><Spin indicator={antIcon}/></div>
    }

    return (
      <div className={s.root}>
        <Space direction="vertical">
          <Title>Информация о карточке</Title>
          <Text>Идентификатор: {1}</Text>
          <Text>Английское: {eng}</Text>
          <Text>Русское: {rus}</Text>
        </Space>
      </div>
    )
  }
}

export default withFirebase(CardInfo)
