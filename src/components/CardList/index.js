import React, {Component} from 'react'
import Card from '../Card'
import getTranslateWord from '../../services/dictionary'

import { Input, Modal } from 'antd'

import s from './CardList.module.scss';
import FirebaseContext from "../../context/fireBaseContext";

const { Search } = Input;

class CardList extends Component {

  state = {
    value: '',
    isBusy: false
  }

  handleInputChange = (e) => {
    this.setState({
      value: e.target.value
    })
  }

  getTheWord = async () => {
    const { value } = this.state
    const getWord = await getTranslateWord(value)
    if (!getWord.ok) {
      Modal.error({
        title: 'Ошибка добавления карточки',
        content: getWord.message,
      })
      this.setState(() => {
        return {
          isBusy: false
        }
      })
      return null
    }

    const newWord = {
      eng: value.toLowerCase(),
      rus: getWord.translate.toLowerCase(),
      id: Date.now()
    }

    this.props.onAddItem(newWord)

    this.setState(() => {
      return {
        value: '',
        isBusy: false
      }
    })
  }

  handleSubmitForm = async () => {
    this.setState({
      isBusy: true
    }, this.getTheWord)
  }

  render() {
    const { items=[], onDeletedItem } = this.props
    const { value, isBusy } = this.state;
    return (
      <>
        <div className="form">
          <Search
            placeholder="Новое слово для изучения (Eng)"
            enterButton="Добавить"
            size="large"
            value={ value }
            onChange={this.handleInputChange}
            onSearch={this.handleSubmitForm}
            loading={isBusy}
          />
        </div>
        <div className={s.root}>
          {
            items.map(({eng, rus, id}) => (
              <Card
                onDeleted={() => onDeletedItem(id)}
                key={id}
                rus={rus}
                eng={eng}
              />
            ))
          }
        </div>
      </>
    )
  }
}

CardList.contextType = FirebaseContext

export default CardList
