import React, {Component} from 'react'
import { BrowserRouter, Switch, Route, Link, Redirect }  from 'react-router-dom'

import LoginPage from './pages/Login'
import HomePage from './pages/Home'
import WordPage from './pages/CardInfo'

import FirebaseContext from "./context/fireBaseContext";

import s from './App.module.scss'

import {Layout, Menu, Spin} from 'antd'
import {LoadingOutlined} from '@ant-design/icons'
import {PrivateRoute} from "./utils/privateRoute";

const {Header, Content} = Layout
const antIcon = <LoadingOutlined style={{fontSize: 100}}/>;

class App extends Component {
  state = {
    user: null
  }

  componentDidMount() {
    const {auth, setUserUid} = this.context
    auth.onAuthStateChanged((user) => {
      if (user) {
        setUserUid(user.uid)
        localStorage.setItem('user', JSON.stringify(user.uid))
        this.setState({
          user,
        })
      } else {
        setUserUid(null)
        localStorage.removeItem('user')
        this.setState({
          user: false,
        })
      }
    })
  }

  render() {
    const {user} = this.state
    const {signOut} = this.context

    if (user === null) {
      return (
        <div className={s.loader_wrap}>
          <Spin indicator={antIcon}/>
        </div>
      )
    }

    return (
      <BrowserRouter>
        <Switch>
          <Route path="/login" component={LoginPage}/>
          <Route render={(props) => {
            const {history: {push}} = props
            return (
              <Layout>
                <Header>
                  <Menu theme="dark" mode="horizontal">
                    <Menu.Item key="1">
                      <Link to="/">Home</Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                      <Link to="/about">About</Link>
                    </Menu.Item>
                    <Menu.Item key="3" onClick={() => push('/contact')}>Contact</Menu.Item>
                    <Menu.Item key="4" onClick={() => {
                      signOut()
                      push('/login')
                    }}>Logout</Menu.Item>
                  </Menu>
                </Header>
                <Content>
                  <Switch>
                    <PrivateRoute path="/" exact component={HomePage}/>
                    <PrivateRoute path="/home" component={HomePage}/>
                    <PrivateRoute path="/word/:id" component={WordPage}/>
                    <Redirect to="/" />
                  </Switch>
                </Content>
              </Layout>
            )
          }
          }/>
        </Switch>
      </BrowserRouter>
    )
  }
}

App.contextType = FirebaseContext

export default App;
