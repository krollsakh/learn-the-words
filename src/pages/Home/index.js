import React, {Component} from "react"
import HeaderBlock from "../../components/HeaderBlock";
import Header from "../../components/Header";
import Paragraph from "../../components/Paragraph";
import CardList from "../../components/CardList";
import {ReactComponent as ReactLogoSvg} from '../../logo.svg'
import FirebaseContext from "../../context/fireBaseContext";

class HomePage extends Component {
  state = {
    wordArr: [],
  }

  componentDidMount() {
    const { getUserCardsRef } = this.context

    getUserCardsRef().on('value', res => {
      this.setState({
        wordArr: res.val() || [],
      })
    })
  }

  handleDeletedItem = (id) => {
    const { getUserCardsRef } = this.context
    const { wordArr } = this.state
    const newWordArr = wordArr.filter((i) => i.id !== id)
    getUserCardsRef().set(newWordArr)
  }

  handleAddItem = (newWord) => {
    const { getUserCardsRef } = this.context
    const { wordArr } = this.state
    getUserCardsRef().set([...wordArr, newWord])
  }

  render() {
    const {wordArr} = this.state;

    return (
      <>
        <HeaderBlock>
          <Header>
            Время учить слова онлайн
          </Header>
          <Paragraph>
            Воспользуйтесь карточками для запоминания и пополнения активного словарного запаса
          </Paragraph>
          <CardList onDeletedItem={this.handleDeletedItem} onAddItem={this.handleAddItem} items={wordArr}/>
        </HeaderBlock>
        <HeaderBlock hideBackground>
          <Header>
            Работа по марафону
          </Header>
          <Paragraph>
            РеактМарафон - rulezz!
          </Paragraph>
          <ReactLogoSvg/>
        </HeaderBlock>
      </>
    )
  }
}

HomePage.contextType = FirebaseContext

export default HomePage
