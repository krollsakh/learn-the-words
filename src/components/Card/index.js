import React from 'react'
import classNames from 'classnames'
import {CheckSquareOutlined, DeleteOutlined} from '@ant-design/icons'

import s from './Card.module.scss'

class Card extends React.Component {

  state = {
    done: false,
    isRemembered: false,
  }

  handleClickCard = () => {
    this.setState((state) => {
      return {
        done: state.isRemembered || !state.done
      }
    })
  }

  handleIsRememberedClick = () => {
    this.setState((isRemembered) => {
      return {
        isRemembered, done: true
      }
    })
  }

  handleDeletedClick = () => {
    this.props.onDeleted()
  }

  render() {
    const {eng, rus} = this.props
    const {done, isRemembered} = this.state;

    return (
      <div className={s.root}>
        <div
          onClick={this.handleClickCard}
          className={classNames(s.card, {
            [s.done]: done,
            [s.isRemembered]: isRemembered
          })}
        >
          <div className={s.cardInner}>
            <div className={s.cardFront}>
              {eng}
            </div>
            <div className={s.cardBack}>
              {rus}
            </div>
          </div>
        </div>
        <div className={s.icons}>
          <CheckSquareOutlined onClick={this.handleIsRememberedClick}/>
        </div>
        <div className={s.icons}>
          <DeleteOutlined onClick={this.handleDeletedClick}/>
        </div>
      </div>
    );
  }
}

export default Card;
