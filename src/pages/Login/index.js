import React, {Component} from 'react'
import {Layout, Form, Input, Button} from 'antd'

import s from './Login.module.scss'
import FirebaseContext from "../../context/fireBaseContext";

const {Content} = Layout

class LoginPage extends Component {

  onFinish = ({email, password}) => {
    const { signWithEmail, setUserUid } = this.context
    const { history } = this.props

    signWithEmail(email, password)
      .then((res) => {
        setUserUid(res.user.uid)
        localStorage.setItem('user', res.user.uid)
        history.push('/')
      })
  };

  onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  renderForm = () => {
    const layout = {
      labelCol: {span: 6},
      wrapperCol: {span: 18},
    };
    const tailLayout = {
      wrapperCol: {offset: 6, span: 18},
    };

    return (
      <Form
        {...layout}
        name="basic"
        onFinish={this.onFinish}
        onFinishFailed={this.onFinishFailed}
      >

        <Form.Item
          label="email"
          name="email"
          rules={[{required: true, message: 'Введите email!'}]}
        >
          <Input
            placeholder="Введите email"
          />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{required: true, message: 'Пожалуйста, введите пароль!'}]}
        >
          <Input.Password
            placeholder="Введите пароль"/>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Войти
          </Button>
        </Form.Item>
      </Form>
    )
  }

  render() {
    return (
      <Layout>
        <Content>
          <div className={s.root}>
            <div className={s.form_wrap}>
              {this.renderForm()}
            </div>
          </div>
        </Content>
      </Layout>
    )
  }
}

LoginPage.contextType = FirebaseContext

export default LoginPage
